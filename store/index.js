// import contentData from '~/data/contentData.json'
// import contentFolders from '~/data/contentFolders.json'
// import { getFolders } from '~/helpers/contentHelper.js'

export const state = () => ({
  content: {},
  index: [],
  folders: []
})

export const mutations = {
  setContent(state, content) {
    state.content = content
  },
  setContentIndex(state, index) {
    state.index = index
  },
  setContentFolders(state, folders) {
    state.folders = folders
  }
}

// export const actions = {
//   nuxtServerInit({ commit }) {
//     commit('setContent', contentData)

//     // const index = getElements(contentData)
//     // commit('setContentIndex', index)

//     // eslint-disable-next-line no-console
//     console.log('init')
//     const folders = getFolders(contentFolders.folders)
//     commit('setContentFolders', folders)
//   }
// }

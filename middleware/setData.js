import contentData from '~/data/contentData.json'
import contentFolders from '~/data/contentFolders.json'
import { getFolders, getElements } from '~/helpers/contentHelper.js'

export default function({ store: { commit } }) {
  commit('setContent', contentData)

  const index = getElements(contentData)
  commit('setContentIndex', index)

  const folders = getFolders(contentFolders.folders)
  commit('setContentFolders', folders)

  // const searchResult = searchData(index, 'aritmetica')
  // // eslint-disable-next-line no-console
  // console.log('init', searchResult)
}

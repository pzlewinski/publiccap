import urlSlug from 'url-slug'

export const getElements = function(contentData) {
  const list = []
  getChild(contentData, list)
  return list
}

function getChild(element, list) {
  if (!element.path) element.path = [element.name]
  else element.path.push(element.name)

  if (element.children && element.children.length > 0) {
    element.children.forEach(children => {
      children.path = [...element.path]
      getChild(children, list)
    })
  } else {
    list.push(element)
  }
}

export const getFolders = function(folderData) {
  folderData.forEach(element => {
    element.id = urlSlug(element.name)
  })
  return folderData
}

function filterData(content, query) {
  const description = urlSlug(content.description).toLowerCase()

  if (description.indexOf(query) > -1) return true
  else if (Array.isArray(content.path) || Array.isArray(content.tags)) {
    const property = content.path || content.tags
    let hasQuery = false

    for (let i = 0; i < property.length; i++) {
      const element = property[i]
      const elementSlug = urlSlug(element).toLowerCase()

      if (elementSlug.indexOf(query) > -1) {
        hasQuery = true
        break
      }
    }
    return hasQuery
  } else return false
}

export const searchData = function(contentList, query) {
  query = urlSlug(query.toLowerCase())
  return contentList.filter(content => {
    return filterData(content, query) === true
  })
}
